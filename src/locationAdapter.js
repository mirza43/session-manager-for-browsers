/**
 * @class {LocationAdapter}
 */
export default class LocationAdapter {

    get href():string {
        return window.location.href;
    }

    set href(value:string) {
        window.location.href = value;
    }

    replace(newUrl:string) {
        window.location.replace(newUrl);
    }

}
