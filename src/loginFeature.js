import {inject} from 'aurelia-dependency-injection';
import LoginUrlFactory from './loginUrlFactory';
import LocationAdapter from './locationAdapter';

/**
 * @class {LoginFeature}
 */
@inject(LoginUrlFactory, LocationAdapter) class LoginFeature {

    _loginUrlFactory:LoginUrlFactory;
    _locationAdapter:LocationAdapter;

    constructor(loginUrlFactory:LoginUrlFactory,
                locationAdapter:LocationAdapter) {

        if (!locationAdapter) {
            throw 'locationAdapter required';
        }
        this._locationAdapter = locationAdapter;

        if (!loginUrlFactory) {
            throw 'loginUrlFactory required';
        }
        this._loginUrlFactory = loginUrlFactory;

    }

    /**
     *
     * Logs a user in by:
     * 1.  redirecting to the configured loginUrl (adds an appropriate RelayState parameter)
     * 2.  setting the resulting access token in browser storage
     */
    execute() {

        this._locationAdapter.href = this._loginUrlFactory.construct();

    }

}

export default LoginFeature;