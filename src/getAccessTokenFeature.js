import {inject} from 'aurelia-dependency-injection';
import StorageAdapter from './storageAdapter';
import LoginFeature from './loginFeature';

/**
 * @class {GetAccessTokenFeature}
 */
@inject(StorageAdapter,LoginFeature)
class GetAccessTokenFeature {

    _storageAdapter:StorageAdapter;
    _loginFeature:LoginFeature;

    constructor(storageAdapter:StorageAdapter,
                loginFeature:LoginFeature) {

        if (!storageAdapter) {
            throw 'storageAdapter required';
        }
        this._storageAdapter = storageAdapter;

        if (!loginFeature) {
            throw 'loginFeature required';
        }
        this._loginFeature = loginFeature;

    }

    /**
     * Retrieves the access_token of the active session. If an active session doesn't exist,
     * initiates a login flow. Primarily used to build an Authorization header to make an API call
     * to a protected resource.
     *
     * @returns {(Promise<string>|Promise<void>)}
     */
    execute(): Promise<string> {

        return this._storageAdapter
            .getAccessToken()
            .catch(() => this._loginFeature.execute());

    }

}

export default GetAccessTokenFeature;
