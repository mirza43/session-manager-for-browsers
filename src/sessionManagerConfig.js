import {IdentityServiceSdkConfig} from 'identity-service-sdk';

/**
 * @class {SessionManagerConfig}
 */
export default class SessionManagerConfig {

    _identityServiceSdkConfig:IdentityServiceSdkConfig;

    _loginUrl:string;

    _logoutUrl:string;

    _accessTokenRefreshInterval:number;

    /**
     * @param {string} identityServiceBaseUrl
     * @param {string} loginUrl
     * @param {string} logoutUrl
     * @param {number} accessTokenRefreshInterval interval (in millisecond) at which the access token will be refreshed. Defaults to 27 minutes
     */
    constructor(identityServiceBaseUrl:string,
                loginUrl:string,
                logoutUrl:string,
                accessTokenRefreshInterval:number = 27 * 60 * 1000) {

        this._identityServiceSdkConfig = new IdentityServiceSdkConfig(identityServiceBaseUrl);

        if (!loginUrl) {
            throw 'loginUrl required';
        }
        this._loginUrl = loginUrl;

        if (!logoutUrl) {
            throw 'logoutUrl required';
        }
        this._logoutUrl = logoutUrl;

        if (!accessTokenRefreshInterval) {
            throw 'accessTokenRefreshInterval required';
        }
        this._accessTokenRefreshInterval = accessTokenRefreshInterval;

    }

    /**
     * @returns {IdentityServiceSdkConfig}
     */
    get identityServiceSdkConfig() {
        return this._identityServiceSdkConfig;
    }

    /**
     * @returns {string}
     */
    get loginUrl() {
        return this._loginUrl;
    }

    /**
     * @returns {string}
     */
    get logoutUrl() {
        return this._logoutUrl;
    }

    /**
     * @returns {number}
     */
    get accessTokenRefreshInterval() {
        return this._accessTokenRefreshInterval;
    }
}