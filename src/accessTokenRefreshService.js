import {inject} from 'aurelia-dependency-injection';
import StorageAdapter from './storageAdapter';
import SessionManagerConfig from './sessionManagerConfig';
import IdentityServiceSdk from 'identity-service-sdk';
import LoginFeature from './loginFeature';

/**
 * @class {AccessTokenRefreshService}
 */
@inject(StorageAdapter,SessionManagerConfig,IdentityServiceSdk,LoginFeature)
class AccessTokenRefreshService {

    constructor(storageAdapter:StorageAdapter,
                config:SessionManagerConfig,
                identityServiceSdk:IdentityServiceSdk,
                loginFeature:LoginFeature) {

        if (!storageAdapter) {
            throw 'storageAdapter required';
        }
        if (!config) {
            throw 'config required';
        }
        if (!identityServiceSdk) {
            throw 'identityServiceSdk required';
        }
        if (!loginFeature) {
            throw 'loginFeature required';
        }

        storageAdapter
            .getAccessToken()
            .then((accessToken) => identityServiceSdk.refreshAccessToken(accessToken))
            .then((accessToken) => storageAdapter.setAccessToken(accessToken))
            .then(() => {
                // set interval which will refresh the access token at the configured interval
                setInterval(() =>
                        storageAdapter.getAccessToken()
                            .then((accessToken) => identityServiceSdk.refreshAccessToken(accessToken))
                            .then((accessToken) => storageAdapter.setAccessToken(accessToken))
                            /*
                             if access token cannot be refreshed, initiate a login flow.
                             */
                            .catch(() => loginFeature.execute()),
                    config.accessTokenRefreshInterval
                );
            });

    }
}

export default AccessTokenRefreshService;