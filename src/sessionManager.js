import DiContainer from './diContainer';
import AccessTokenRefreshService from './accessTokenRefreshService';
import GetAccessTokenFeature from './getAccessTokenFeature';
import GetUserInfoFeature from './getUserInfoFeature';
import LoginFeature from './loginFeature';
import LogoutFeature from './logoutFeature';
import SessionManagerConfig from './sessionManagerConfig';

/**
 * @class {SessionManager}
 * @borrows GetAccessTokenFeature#execute as getAccessToken
 *
 */
export default class SessionManager {

    _diContainer:DiContainer;

    /**
     * @param {SessionManagerConfig} config
     */
    constructor(config:SessionManagerConfig) {

        if (!config) {
            throw 'config required';
        }
        this._diContainer = new DiContainer(config);

        // init AccessTokenRefreshService
        this._diContainer.get(AccessTokenRefreshService);

    }

    getAccessToken() {

        return this
            ._diContainer
            .get(GetAccessTokenFeature)
            .execute();

    }

    getUserInfo() {

        return this
            ._diContainer
            .get(GetUserInfoFeature)
            .execute();

    }

    login() {

        return this
            ._diContainer
            .get(LoginFeature)
            .execute();

    }

    logout() {

        return this
            ._diContainer
            .get(LogoutFeature)
            .execute();

    }

}