Feature: Get user info
  Gets userInfo for the active session. If an active session doesn't exist or is invalid, initiates a login flow

  Background:
    Given partnerRepOidcUserInfo consists of:
      | attribute       | validation              | type   |
      | givenName       | required                | string |
      | familyName      | required                | string |
      | email           | required                | string |
      | sub             | required                | string |
      | accountId       | required                | string |
      | sapVendorNumber | optional                | string |
      | type            | must equal "partnerRep" | string |
    And employeeOidcUserInfo consists of:
      | attribute  | validation            | type   |
      | givenName  | required              | string |
      | familyName | required              | string |
      | email      | required              | string |
      | type       | must equal "employee" | string |

  Scenario Outline: Logged in
    Given I am logged in as <userType>
    When I execute getUserInfo
    Then my <oidcUserInfoType> is returned

    Examples:
      | userType   | oidcUserInfoType           |
      | partnerRep | partnerRepOidcUserInfo |
      | employee   | employeeOidcUserInfo   |